import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {StudentService} from "../student.service";
import {switchMap} from "rxjs";

@Component({
  selector: 'app-single-student',
  templateUrl: './single-student.component.html',
  styleUrls: ['./single-student.component.css']
})
export class SingleStudentComponent implements OnInit {
  private student?: Student;

  constructor(private service:StudentService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => this.service.getOne(params['id']))
    )
      .subscribe(data => this.student = data)
  }

}
