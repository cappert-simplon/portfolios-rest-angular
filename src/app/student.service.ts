import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http:HttpClient) { }

  getAll() {
    return this.http.get<Student[]>(environment.apiUrl + 'student/');
  }

  getBySession(session: string) {
    return this.http.get<Student>(environment.apiUrl + 'student?session=' + session)
  }

  getOne(id: number) {
    return this.http.get<Student>('student/'+ id);
  }
}
