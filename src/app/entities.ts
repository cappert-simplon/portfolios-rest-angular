interface Student {
  id?: number;
  name: string;
  email: string;
  session: string;
}

interface Project {
  id?: number;
  techs: string;
  picture: string;
  student: Student;
}
