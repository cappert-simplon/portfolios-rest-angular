import {Component, Input, OnInit} from '@angular/core';
import {StudentService} from "../student.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-student-card',
  templateUrl: './student-card.component.html',
  styleUrls: ['./student-card.component.css']
})
export class StudentCardComponent implements OnInit {

  @Input()
  student?: Student;

  constructor(private studentservice: StudentService, private route:ActivatedRoute) { }

  ngOnInit(): void {
  }

}
