# Appli angular et affichage des students
1. Créer une appli angular-portfolio avec le routing
2. Dans le index.html, mettre le link de bootstrap
3. Dans le src/app créer un fichier entities.ts et dedans créer des interfaces pour Student et pour Project, en faisant en sorte que l'id soit optionnel pour les deux
4. Générer un component HomeComponent et lui créer une route dans le AppRoutingModule pour faire que ça soit la page d'accueil par défaut
5. Générer un StudentService et dedans mettre les deux méthodes pour le getAll et pour le getOne (donc ajouter le HttpClientModule, injecter le HttpClient dans le service etc.)
6. Dans le HomeComponent, injecter le StudentService et l'utiliser pour aller chercher la liste des students qu'on assigne à une propriété
7. Générer un component StudentComponent qui va avoir en propriété un Student avec un @Input pour faire qu'il vienne de l'extérieur
8. Dans le template home.component.html, faire une boucle qui va afficher un app-student à chaque tour
9. Faire le templating pour que ça affiche les student sous forme de cards boostrap par exemple 

## Une manière de commencer un projet angular :
* Identifier les différentes pages de l'application
* Identifier ce qui sera commun à toutes les pages (cette étape et la précédente justifient à elles seules la création de maquette fonctionnelles)
* Créer un component par page et lui assigner une route dans le AppRoutingModule
* Ajouter dans le app.component.html autour du <router-outlet> les éléments d'interface qui seront communs à toutes les pages
* Faire le html et le CSS des différentes pages, avec du contenu en dur éventuellement
* Créer les interfaces/entités pour le typage
* Créer les services pour les entités avec les méthodes pour les différentes routes dont on aura besoin parmi celles déclarées côté spring
* Injecter le ou les services dans les pages où ils seront utilisés et faire en sorte de remplacer le contenu en dur par des données venant du backend
* Rajouter les interactions avec l'interfaces (les formulaires, les events, etc.) et les lier à des méthode des services
￼
## Afficher une page pour un Student
1. Générer un component SingleStudent
2. Créer une route pour le SingleStudentComponent dans le AppRoutingModule, et faire que cette route attende un paramètre, nous on veut que la route ça soit student/:id où id sera le paramètre dynamique
3. Dans le SingleStudentComponent, injecter dans le constructor un paramètre de type ActivatedRoute
4. Dans le ngOnInit utilisé le ActivatedRoute faire un subscribe sur la propriété params et dans ce subscribe, récupérer l'id et en faire un console.log pour le moment
5. Dans le student-card.component.html, mettre un [routerLink] sur le lien See projects et faire qu'il pointe vers '/student/' concaténé avec l'id du student
6. Une fois que vous arrivez à récupérer l'id, injecter le StudentService dans le constructor, et déclarer une propriété student?:Student
7. À la place de votre console.log déclencher un getById et se subscribe dessus et assigner le data au student (c'est pas la meilleure manière de faire, mais on regardera une façon mieux après)

### Template du Student et des Projects
1. Maintenant qu'on a le student dans SingleStudentComponent, faire le template de celui ci pour afficher les différentes informations du student
2. Générer un component ProjectCard et faire exactement comme on a fait avec StudentCard (avec input et tout) pour afficher les informations du projet sous forme de card bootstrap (ptite différence, ça sera une card avec une image)
3. Dans le template du single-student, faire une boucle sur les student.projects et dans cette boucle afficher des app-project-card

## findBySession
### Côté Back
1. Modifier la route getAll du StudentController en lui rajoutant un RequestParam optionnel dans ses arguments, qu'on appellera session et qui sera du String
2. Dans la méthode, vérifier si l'argument session est null, si oui, on fait le findAll comme avant, si non, on fait un findBySession
3. Vous pouvez tester si ça marche en relançant le testGetAll et le testGetByPromo

### Côté Front
1. Créer une nouvelle méthode dans le StudentService qui va attendre une string session en argument et qui fera une requête vers /api/student?session=...
2. Générer un nouveau component SessionComponent et le lier à une route paramétrée session/:session
